/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.Queue;

/**
 *
 * @author ngrajales
 */
@Named(value = "clasificadorBean")
@RequestScoped
public class ClasificadorBean {

    @Resource(mappedName = "jms/ejercicio5")
    private Queue ejercicio5;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
    

    @Inject
    @JMSConnectionFactory("java:comp/DefaultJMSConnectionFactory")
    private JMSContext context;

    /**
     * Creates a new instance of ClasificadorBean
     */
    public ClasificadorBean() {
    }

    private void sendJMSMessageToEjercicio5(String messageData) {
        context.createProducer().send(ejercicio5, messageData);
    }
    
    public void enviar(){
        this.sendJMSMessageToEjercicio5(this.name); 
    }
    
}
