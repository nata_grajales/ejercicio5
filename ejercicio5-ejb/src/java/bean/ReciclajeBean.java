/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 *
 * @author ngrajales
 */
@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/ejercicio5")
    ,
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class ReciclajeBean implements MessageListener {

    public ReciclajeBean() {
    }

    @Override
    public void onMessage(Message message) {
        try {
            TextMessage mensaje = (TextMessage) message;

            if (mensaje != null) {
                System.out.println("mensaje = " + mensaje.getText());
            }

        } catch (JMSException ex) {
            System.out.println(
                    "Error mensaje");
        }
    }

}
